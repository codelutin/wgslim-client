#!/bin/bash

DIR="$(dirname "$(realpath "$0")")"

source "$DIR/lib/utils.sh"
source "$DIR/lib/generator.sh"

source "$DIR/config/wgslim.conf.sh" || err "config file not found"

reconfigure_client
