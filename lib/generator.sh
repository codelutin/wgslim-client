# reconfigure_wg
# sets: -
reconfigure_wg() {
	needs INTERFACE_NAME IPLIST_FILE WG_CONFIG_DIR CLIENT_PRIV_KEY SERVER_PUB_KEY PRESHARED_KEY FREE_IPv4 FREE_IPv6 ENDPOINT DNS DNS_SEARCH

	info "Trying to turn down interface $INTERFACE_NAME"
	wg-quick down "$INTERFACE_NAME"

	cat <<- EOF | overwrite_file_as_root "$WG_CONFIG_DIR/$INTERFACE_NAME.conf"
	[Interface]
	PrivateKey = $CLIENT_PRIV_KEY
	Address = $FREE_IPv4/32
	Address = $FREE_IPv6/128
	DNS = $DNS
	DNS = $DNS_SEARCH

	[Peer]
	PublicKey = $SERVER_PUB_KEY
	PresharedKey = $PRESHARED_KEY
	Endpoint = $ENDPOINT
	EOF
	while read IP; do
		echo "AllowedIPs = $IP" | append_to_file_as_root "$WG_CONFIG_DIR/$INTERFACE_NAME.conf"
	done < <(grep -Eo "^[^# ]*" "$IPLIST_FILE")

	info "Trying to bring up interface $INTERFACE_NAME"
	wg-quick up "$INTERFACE_NAME"
}

# reconfigure_nmcli
# sets: -
reconfigure_nmcli() {
	needs INTERFACE_NAME IPLIST_FILE NM_CONFIG_DIR CLIENT_PRIV_KEY SERVER_PUB_KEY PRESHARED_KEY FREE_IPv4 FREE_IPv6 ENDPOINT DNS DNS_SEARCH

	cat <<- EOF | overwrite_file_as_root "$NM_CONFIG_DIR/system-connections/$INTERFACE_NAME.nmconnection"
	[connection]
	id=$INTERFACE_NAME
	uuid=$(uuidgen)
	type=wireguard
	interface-name=$INTERFACE_NAME
	permissions=

	[wireguard]
	private-key=$CLIENT_PRIV_KEY

	[wireguard-peer.$SERVER_PUB_KEY]
	endpoint=$ENDPOINT
	preshared-key=$PRESHARED_KEY
	preshared-key-flags=0
	allowed-ips=$(grep -Eo "^[^# ]*" "$IPLIST_FILE" | tr '\n' ';')

	[ipv4]
	address1=$FREE_IPv4/32
	dns=$DNS;
	dns-priority=-50
	dns-search=$DNS_SEARCH;
	method=manual

	[ipv6]
	addr-gen-mode=stable-privacy
	address1=$FREE_IPv6/128
	dns-priority=-50
	dns-search=$DNS_SEARCH;
	method=manual

	[proxy]
	EOF

	info "Trying to load connection with nmcli"
	run_as_root nmcli connection load "$NM_CONFIG_DIR/system-connections/$INTERFACE_NAME.nmconnection"
}

# generate_keys
# needs: -
# sets: CLIENT_PRIV_KEY CLIENT_PUB_KEY PRESHARED_KEY
generate_keys() {
	CLIENT_PRIV_KEY=$(wg genkey)
	CLIENT_PUB_KEY=$(echo $CLIENT_PRIV_KEY | wg pubkey)
	PRESHARED_KEY=$(wg genpsk)
}

# request_config
# sets: SERVER_PUB_KEY FREE_IPv4 FREE_IPv6 ENDPOINT DNS DNS_SEARCH
request_config() {
	needs SSH_DEST ID COMMENT CLIENT_PUB_KEY PRESHARED_KEY
	info "running ssh $SSH_DEST wgslim_gen.sh $ID $COMMENT $CLIENT_PUB_KEY $PRESHARED_KEY"
	local RESULT="$(ssh "$SSH_DEST" wgslim_gen.sh "$ID" "$COMMENT" "$CLIENT_PUB_KEY" "$PRESHARED_KEY")"
	# Sanitize input
	# Wireguard keys are base64-encoded (source: https://www.wireguard.com/quickstart/), so can contain [a-zA-Z0-9+/=]
	# '.' is required for IPv4 addresses, ':' for IPv6 addresses and ';' is the separator
	grep -qE "[^a-zA-Z0-9+/=.:;]" < <(echo "$RESULT") && err "Found unexpected character. Allowed characters: [a-zA-Z0-9+/=.:;]. Output by server (may contain invisible characters): $RESULT"
	IFS=";" read RESPONSE_VERSION SERVER_PUB_KEY FREE_IPv4 FREE_IPv6 ENDPOINT DNS DNS_SEARCH < <(echo "$RESULT")
	[ "$RESPONSE_VERSION" == "0.2" ] || err "unsupported RESPONSE_VERSION: $RESPONSE_VERSION"
}

# reconfigure_client
# sets: -
reconfigure_client() {
	needs SSH_DEST ID COMMENT INTERFACE_NAME IPLIST_FILE WG_CONFIG_DIR NM_CONFIG_DIR

	info "testing prerequisites..."
	test_wg_support

	if ! nmcli_supported; then
		warn "falling back to wireguard-tools + resolvconf"
		if ! command -v resolvconf > /dev/null; then
			err "This tool requires NetworkManager or resolvconf"
		fi
	fi

	[ ! -f "$IPLIST_FILE" ] && err "file $IPLIST_FILE does not exist (or not a regular file)"
	[ -n "$WG_CONFIG_DIR" -a ! -d "$WG_CONFIG_DIR" ] && err "dir $WG_CONFIG_DIR does not exist"
	[ -n "$NM_CONFIG_DIR" -a ! -d "$NM_CONFIG_DIR" ] && err "dir $NM_CONFIG_DIR does not exist"

	local CLIENT_PRIV_KEY CLIENT_PUB_KEY PRESHARED_KEY SERVER_PUB_KEY FREE_IPv4 FREE_IPv6 ENDPOINT DNS DNS_SEARCH
	generate_keys
	request_config

	if [ "$NMCLI_SUPPORTED" == "true" ]; then
		reconfigure_nmcli
	else
		reconfigure_wg
	fi
}
