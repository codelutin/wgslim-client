Include lib/generator.sh
Include lib/utils.sh

SHELLSPEC_TESTING="true"

Describe 'Test request_config()'
	setup() {
		SSH_DEST="server.tld"
		ID="61aef0410aae422398b26330b372da22"
		COMMENT="test"
		CLIENT_PUB_KEY="HX0RNP5BPBfpsPXoM6VLOMS6G8bTOVzZRg8h8Ot5eWs="
		PRESHARED_KEY="z8lYAUkyRv+CSbPcMM7K8vMt9ZRK/spjI6BEhKRpNsc="
	}
	Before setup

	It 'calls request_config()'
		ssh() {
			echo "0.2;0aLkSpBTP6Kadr//Bj3QtRcbaLTlvQUsV8DPPD6rUQ4=;10.0.0.3;fd70::1c;server.tld:51820;192.168.10.1;dns.local"
		}
		When call request_config
		The status should be success
		The output should equal "$(info 'running ssh server.tld wgslim_gen.sh 61aef0410aae422398b26330b372da22 test HX0RNP5BPBfpsPXoM6VLOMS6G8bTOVzZRg8h8Ot5eWs= z8lYAUkyRv+CSbPcMM7K8vMt9ZRK/spjI6BEhKRpNsc=')"
	End

	It 'calls request_config() with bad input (backslash in endpoint)'
		ssh() {
			echo "0.2;0aLkSpBTP6Kadr//Bj3QtRcbaLTlvQUsV8DPPD6rUQ4=;10.0.0.3;fd70::1c;server.tld\wg:51820;192.168.10.1;dns.local"
		}
		expected_output() {
			info 'running ssh server.tld wgslim_gen.sh 61aef0410aae422398b26330b372da22 test HX0RNP5BPBfpsPXoM6VLOMS6G8bTOVzZRg8h8Ot5eWs= z8lYAUkyRv+CSbPcMM7K8vMt9ZRK/spjI6BEhKRpNsc='
			testerr 'Found unexpected character. Allowed characters: [a-zA-Z0-9+/=.:;]. Output by server (may contain invisible characters): 0.2;0aLkSpBTP6Kadr//Bj3QtRcbaLTlvQUsV8DPPD6rUQ4=;10.0.0.3;fd70::1c;server.tld\wg:51820;192.168.10.1;dns.local'
		}
		When run request_config
		The status should be failure
		The output should equal "$(expected_output)"
	End

End

Describe 'Test reconfigure_client()'
	request_config() {
		SERVER_PUB_KEY="0aLkSpBTP6Kadr//Bj3QtRcbaLTlvQUsV8DPPD6rUQ4="
		FREE_IPv4="10.0.0.2"
		FREE_IPv6="fd70::2"
		ENDPOINT="server.tld:51820"
		DNS="192.168.10.1"
		DNS_SEARCH="dns.local"
	}
	generate_keys() {
		CLIENT_PRIV_KEY="ECaIpW96ankhJpeAruFqavBZxX3plBshG8AJHQXUH0A="
		CLIENT_PUB_KEY="HX0RNP5BPBfpsPXoM6VLOMS6G8bTOVzZRg8h8Ot5eWs="
		PRESHARED_KEY="z8lYAUkyRv+CSbPcMM7K8vMt9ZRK/spjI6BEhKRpNsc="
	}
	setup(){
		FAKE_TEST_DIR="$(mktemp -d)"

		SSH_DEST="server.tld"
		ID="61aef0410aae422398b26330b372da22"
		COMMENT="test"
		INTERFACE_NAME="wg"
		IPLIST_FILE="config/iplist.template"
		WG_CONFIG_DIR="$FAKE_TEST_DIR/wireguard"
		NM_CONFIG_DIR="$FAKE_TEST_DIR/NetworkManager"

		mkdir -p "$WG_CONFIG_DIR" "$NM_CONFIG_DIR"
	}
	cleanup() {
		rm -r "$FAKE_TEST_DIR"
	}

	Before setup
	After cleanup

	It 'calls reconfigure_client() with wireguard-tools but not resolvconf or nmcli'
		command() {
			[ "$2" == "nmcli" ] && return 1
			[ "$2" == "resolvconf" ] && return 1
			[ "$2" == "wg" ] && return 0
		}
		wg() {
			echo "wireguard-tools v1.0.20210914 - https://git.zx2c4.com/wireguard-tools/"
		}
		Mock wg-quick
		End
		expected_output() {
			info 'testing prerequisites...'
			warn 'nmcli not found'
			warn 'falling back to wireguard-tools + resolvconf'
			testerr 'This tool requires NetworkManager or resolvconf'
		}
		When run reconfigure_client
		The status should be failure
		The output should equal "$(expected_output)"
	End

	It 'calls reconfigure_client() with unsupported nmcli but fallback to wireguard-tools + resolvconf'
		nmcli() {
			if [ "$1" == "--version" ]; then
				echo "nmcli tool, version 1.8.2"
			else
				return 0
			fi
		}
		wg() {
			echo "wireguard-tools v1.0.20210914 - https://git.zx2c4.com/wireguard-tools/"
		}
		Mock wg-quick
		End
		resolvconf() {
			echo ""
		}
		expected_output() {
			info 'testing prerequisites...'
			warn 'nmcli version 1.8.2 not supported (should be 1.x and >= 1.22)'
			warn 'falling back to wireguard-tools + resolvconf'
			info 'Trying to turn down interface wg'
			info "overwriting $FAKE_TEST_DIR/wireguard/wg.conf"
			info "appending to $FAKE_TEST_DIR/wireguard/wg.conf"
			info "appending to $FAKE_TEST_DIR/wireguard/wg.conf"
			info "appending to $FAKE_TEST_DIR/wireguard/wg.conf"
			info 'Trying to bring up interface wg'
		}
		wg_conf_expected_contents() {
			%text
			#|[Interface]
			#|PrivateKey = ECaIpW96ankhJpeAruFqavBZxX3plBshG8AJHQXUH0A=
			#|Address = 10.0.0.2/32
			#|Address = fd70::2/128
			#|DNS = 192.168.10.1
			#|DNS = dns.local
			#|
			#|[Peer]
			#|PublicKey = 0aLkSpBTP6Kadr//Bj3QtRcbaLTlvQUsV8DPPD6rUQ4=
			#|PresharedKey = z8lYAUkyRv+CSbPcMM7K8vMt9ZRK/spjI6BEhKRpNsc=
			#|Endpoint = server.tld:51820
			#|AllowedIPs = 10.0.0.1
			#|AllowedIPs = fd70::1
			#|AllowedIPs = 192.168.10.0/24
		}
		When call reconfigure_client
		The status should be success
		The output should equal "$(expected_output)"
		The contents of file "$FAKE_TEST_DIR/wireguard/wg.conf" should equal "$(wg_conf_expected_contents)"
	End

	It 'calls reconfigure_client() with supported nmcli'
		nmcli() {
			if [ "$1" == "--version" ]; then
				echo "nmcli tool, version 1.22.10"
			else
				return 0
			fi
		}
		uuidgen() {
			echo "5bda3d58-ebc6-4ad8-9c93-2af61b42a611"
		}
		wg_conf_expected_contents() {
			%text
			#|[connection]
			#|id=wg
			#|uuid=5bda3d58-ebc6-4ad8-9c93-2af61b42a611
			#|type=wireguard
			#|interface-name=wg
			#|permissions=
			#|
			#|[wireguard]
			#|private-key=ECaIpW96ankhJpeAruFqavBZxX3plBshG8AJHQXUH0A=
			#|
			#|[wireguard-peer.0aLkSpBTP6Kadr//Bj3QtRcbaLTlvQUsV8DPPD6rUQ4=]
			#|endpoint=server.tld:51820
			#|preshared-key=z8lYAUkyRv+CSbPcMM7K8vMt9ZRK/spjI6BEhKRpNsc=
			#|preshared-key-flags=0
			#|allowed-ips=10.0.0.1;fd70::1;192.168.10.0/24;
			#|
			#|[ipv4]
			#|address1=10.0.0.2/32
			#|dns=192.168.10.1;
			#|dns-priority=-50
			#|dns-search=dns.local;
			#|method=manual
			#|
			#|[ipv6]
			#|addr-gen-mode=stable-privacy
			#|address1=fd70::2/128
			#|dns-priority=-50
			#|dns-search=dns.local;
			#|method=manual
			#|
			#|[proxy]
		}
		When call reconfigure_client
		The status should be success
		The contents of file "$FAKE_TEST_DIR/NetworkManager/system-connections/wg.nmconnection" should equal "$(wg_conf_expected_contents)"
		The line 1 of output should equal "$(info 'testing prerequisites...')"
		The line 2 of output should equal "$(info "overwriting $FAKE_TEST_DIR/NetworkManager/system-connections/wg.nmconnection")"
		The line 3 of output should end with "'$FAKE_TEST_DIR/NetworkManager/system-connections'" # GNU coreutils: 'mkdir: created directory xxxx' / Busybox: 'created directory: xxxx'
		The line 4 of output should end with "$(info 'Trying to load connection with nmcli')"
	End
End
