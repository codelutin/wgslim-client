########## User config ##########
# Required by run.sh
SSH_DEST=""
ID="$(cat /etc/machine-id)"
COMMENT="$(hostname)"
# Required by reconfigure_client()
INTERFACE_NAME="wg"
IPLIST_FILE="$DIR/config/iplist"
#################################

# No need to change the following vars unless we are testing
WG_CONFIG_DIR="/etc/wireguard"
NM_CONFIG_DIR="/etc/NetworkManager"
